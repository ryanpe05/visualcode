# allow imports from parent directory
import sys
import os
sys.path.insert(1, os.path.join(sys.path[0], '..'))

import unittest
from visualCode import VisualCode

class SyntaxTesting(unittest.TestCase):

	def setUp(self):
		github_url = "https://github.com/ryanpe05/visualcode.git"
		github_weighting = 0.5
		language = "Python"
		dir_names = github_url.split('/')
		project_name = dir_names[-1].replace('.git', '')
		need_download = True
		for path in os.listdir('.'):
			if path == project_name:
				need_download = False
		if need_download:
			os.system("git clone {}".format(github_url))
		use_github = False
		self.myGraphObject = VisualCode(project_name, use_github, github_weighting)

	def tearDown(self):
		pass

	def testClasses(self):
		found_classes = self.myGraphObject.class_list.keys()
		self.visual_classes = ["VisualCode"]
		self.test_classes = ["SyntaxTesting"]
		expected_classes = self.visual_classes + self.test_classes
		extra_found, not_found = self.compare_lists(found_classes, expected_classes)
		assert not extra_found, "Found extra classes: {}".format(str(extra_found))
		assert not not_found, "Could not find expected classes: {}".format(str(not_found))

	def testFunctions(self):
		found_funcs = self.myGraphObject.func_list.keys()
		self.backend_funcs = ["hello_world", "generate_graph"]
		self.visual_funcs = ["main", "parseFile", "findUses", "searchFile",
			"fillNetwork", "useGitHub", "convertToJSON", "findComments"]
		self.test_funcs = ["tearDown", "setUp", "testClasses", "testFunctions"]
		expected_funcs = self.backend_funcs + self.visual_funcs + self.test_funcs
		extra_found, not_found = self.compare_lists(found_funcs, expected_funcs)
		assert not extra_found, "Found extra functions: {}".format(str(extra_found))
		assert not not_found, "Could not find expected functions: {}".format(str(not_found))

	def compare_lists(self, found, expectation):
		extra_found = []
		for ind_obj in found:
			if ind_obj in expectation:
				expectation.remove(ind_obj)
			else:
				extra_found.append(ind_obj)
		# Now everything left over wasn't found
		# This prevents miscounting repeated classes
		return extra_found, expectation


if __name__ == "__main__":
	unittest.main() # run all tests
